#!/usr/bin/env python3
import argparse
import asyncio
import sys
import enum
from functools import partial


class States(enum.Enum):
    UP = 1
    DOWN = 2
    OPEN = 3
    IDLE = 4


def parse_arguments():
    def validate_range(t, from_, to, value):
        v = t(value)
        if (from_ and v < from_) or (to and v > to):
            raise argparse.ArgumentTypeError(
                'Value {} must be in range {}-{}'.format(value, from_, to)
                )
        return v

    floors_type = partial(validate_range, int, 5, 20)
    positive_float = partial(validate_range, float, 0.001, None)

    parser = argparse.ArgumentParser(description='Business center lift simulator')
    parser.add_argument('-f', '--floors', help='Floors count in building',
                        type=floors_type, default=20)
    # "p" stands for "peak". Avoiding conflict with `-h` option
    parser.add_argument('-p', '--height', help='Floors height',
                        type=positive_float, default=3.0)
    parser.add_argument('-s', '--speed', help='Lifts speed',
                        type=positive_float, default=1.5)
    parser.add_argument('-t', '--time', help='Time to open and close doors',
                        type=positive_float, default=4.0)
    args = parser.parse_args()
    return args


class Lift:

    def __init__(self, floor_time, stop_time=1, state=States.IDLE,
                 current_floor=1):
        """
        Describe lift, its states, transitions actions and notification logic.

        Attributes:
            floor_time  time for floor stop
            stop_time   time for lift stop
            want_up     dict of floors, where lift was called to go up
            want_down   dict of floors, where lift was called to go down
            inner_stops dict of floors, which were choose by users inside lift
            top         highest floor
            bottom      lowest floor
            state       lift state
            current_floor   store current floor
        """
        self.want_up = {}
        self.want_down = {}
        self.inner_stops = {}
        self.top = None
        self.bottom = None
        self.state = state
        self.current_floor = current_floor
        self.floor_time = floor_time
        self.stop_time = stop_time

        self.transitions = {
            (States.IDLE, States.OPEN, ): self.open_same_floor,
            (States.OPEN, States.IDLE, ): self.go_idle,
            (States.OPEN, States.UP, ): self.close_go_up,
            (States.OPEN, States.DOWN, ): self.close_go_down,
            (States.IDLE, States.UP, ): self.go_up,
            (States.IDLE, States.DOWN, ): self.go_down,
            (States.UP, States.OPEN, ): self.open_door_up,
            (States.DOWN, States.OPEN, ): self.open_door_down,
            (States.UP, States.DOWN, ): self.change_direction_up_down,
            (States.DOWN, States.UP, ): self.change_direction_down_up,
            # impossible:
            # (States.UP, States.IDLE, )
            # (States.DOWN, States.IDLE, )
        }
        self.processors = {
            States.UP: self.process_up,
            States.DOWN: self.process_down,
            States.IDLE: self.process_idle,
            States.OPEN: self.process_open
        }

    async def make_transition(self, state_to, *args, **kwargs):
        return await self.transitions[self.state, state_to](*args, **kwargs)

    async def process(self, *args, **kwargs):
        return await self.processors[self.state](*args, **kwargs)

    async def open_same_floor(self, user_state):
        self.state = States.OPEN
        await self.open_door()
        self._reset_top_bottom_floor()
        # Ideally, there should be FIFO priority, because previous lift state
        # is IDLE.
        await self.next_waiting_floor_up()

    async def go_idle(self):
        self.state = States.IDLE

    async def close_go_down(self):
        self.state = States.DOWN

    async def close_go_up(self):
        self.state = States.UP

    async def go_down(self, *args, **kwargs):
        self.state = States.DOWN

    async def go_up(self, *args, **kwargs):
        self.state = States.UP

    async def open_door_up(self):
        floor = self.current_floor

        self.state = States.OPEN
        await self.open_door()
        self._reset_top_bottom_floor()
        self.want_up.pop(floor, None)

        await self.next_waiting_floor_up()

    async def open_door_down(self):
        floor = self.current_floor

        self.state = States.OPEN
        await self.open_door()
        self._reset_top_bottom_floor()
        self.want_down.pop(floor, None)

        await self.next_waiting_floor_down()

    def _reset_top_bottom_floor(self):
        """
        Remove current maximums (if required).
        """
        floor = self.current_floor

        if self.top == floor:
            self.top = None
        if self.bottom == floor:
            self.bottom = None
        self.inner_stops.pop(floor, None)

    async def open_door(self):
        """
        After door closed, all inner stops for this floor removed.
        """
        self.notify_opened_door()
        await asyncio.sleep(self.stop_time)
        self.inner_stops.pop(self.current_floor, None)
        self.notify_closed_door()

    async def change_direction_up_down(self):
        self.state = States.DOWN
        await self.make_transition(States.OPEN)

    async def change_direction_down_up(self):
        self.state = States.UP
        await self.make_transition(States.OPEN)

    async def process_up(self):
        floor = self.current_floor

        if self.want_up.get(floor) \
           or self.inner_stops.get(floor):
            await self.make_transition(States.OPEN)
        elif self.top == floor:     # e.g. someone from upper floor
                                    # called lift to floor below
            await self.make_transition(States.DOWN)
        else:
            self.notify()
            await asyncio.sleep(self.floor_time)
            self.current_floor += 1

    async def process_down(self):
        floor = self.current_floor
        if self.want_down.get(floor) or self.inner_stops.get(floor):
            await self.make_transition(States.OPEN)
        elif self.bottom == floor:
            await self.make_transition(States.UP)
        else:
            self.notify()
            await asyncio.sleep(self.floor_time)
            self.current_floor -= 1

    async def process_idle(self):
        floor = self.current_floor
        # very specific case. If two directions were enabled, but user
        # decided not to push any buttons inside: we need to open door for
        # second user (opposite direction)
        if self.want_up.pop(floor, None):
            user_state = States.UP
            await self.make_transition(States.OPEN, user_state)
        elif self.want_down.pop(floor, None):
            user_state = States.DOWN
            await self.make_transition(States.OPEN, user_state)

    async def process_open(self):
        pass

    def call_to_floor(self, floor, state):
        self.top = self.top and max(self.top, floor) or floor
        self.bottom = self.bottom and min(self.bottom, floor) or floor
        if state == States.UP:
            self.want_up[floor] = True
        else:
            self.want_down[floor] = True

    def call_floor(self, floor):
        self.top = self.top and max(self.top, floor) or floor
        self.bottom = self.bottom and min(self.bottom, floor) or floor
        self.inner_stops[floor] = True

    async def next_waiting_floor_up(self):
        """
        If we have some waiting floors (up is priority), go there.
        """
        if self.top and self.top > self.current_floor:
            await self.make_transition(States.UP)
        elif self.bottom and self.bottom < self.current_floor:
            await self.make_transition(States.DOWN)
        else:
            # no top, no bottom -> idle
            await self.make_transition(States.IDLE)

    async def next_waiting_floor_down(self):
        """
        If we have some waiting floors (down is priority), go there.
        """
        if self.bottom and self.bottom < self.current_floor:
            await self.make_transition(States.DOWN)
        elif self.top and self.top > self.current_floor:
            await self.make_transition(States.UP)
        else:
            # no top, no bottom -> idle
            await self.make_transition(States.IDLE)

    def determine_direction(self, floor):
        if floor > self.current_floor:
            return States.UP
        elif floor < self.current_floor:
            return States.DOWN
        else:
            return States.OPEN

    def notify(self):
        s, f = self.state, self.current_floor
        if s == States.UP:
            print('Lift went up through {} floor'.format(f))
        elif s == States.DOWN:
            print('Lift went down through {} floor'.format(f))

    def notify_opened_door(self):
        print('Opened door on floor {}'.format(self.current_floor))

    def notify_closed_door(self):
        print('Closed door on floor {}'.format(self.current_floor))

    def __str__(self):
        return 'state: {}, current: {}, top: {}, bottom: {}, '\
               'stops: {}, {}, {}'.format(
                self.state, self.current_floor, self.top, self.bottom,
                self.want_up, self.want_down, self.inner_stops)


def validate_floor_number(raw_value, floors):
    command, value = raw_value[0], raw_value[1:]
    command = command.upper()
    value = int(value)

    if value < 1 or value > floors:
        raise RuntimeError('Value outside available range (1-{})'
                           .format(floors))

    if (value == 1 and command == 'D') or \
       (value == floors and command == 'U'):
        raise RuntimeError('Cannot call lift below or higher limitations')

    return command, value


def read_input(lift, floors, action_queue):
    value = sys.stdin.readline()
    if not value:
        return

    command, value = validate_floor_number(value, floors)

    is_idle = lift.state == States.IDLE
    state = None

    if command == 'U':
        state = States.UP
        lift.call_to_floor(value, state)
    elif command == 'D':
        state = States.DOWN
        lift.call_to_floor(value, state)
    elif command == 'I':
        lift.call_floor(value)

    if is_idle:
        asyncio.ensure_future(action_queue.put((state, value, )))


async def next_action(lift, action_queue):
    while True:
        if lift.state == States.UP:
            await lift.process()
        elif lift.state == States.DOWN:
            await lift.process()
        elif lift.state == States.OPEN:
            await lift.process()
        else:
            await lift.process()
            user_state, value = await action_queue.get()
            state_toward_direction = lift.determine_direction(value)
            await lift.make_transition(state_toward_direction, user_state)


def main():
    args = parse_arguments()
    floor_time = args.height / args.speed
    stop_time = args.time
    floors = args.floors

    event_loop = asyncio.get_event_loop()

    lift = Lift(floor_time=floor_time, stop_time=stop_time)
    action_queue = asyncio.Queue()
    print("Ready to accept commands")
    event_loop.add_reader(sys.stdin, read_input, lift, floors,
                          action_queue)
    event_loop.create_task(next_action(lift, action_queue))
    event_loop.run_forever()


if __name__ == '__main__':
    main()
