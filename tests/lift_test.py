import pytest
import asyncio

from business_lift import Lift, next_action, States


# we use this exception to break loop execution
class OKException(Exception):
    pass


@pytest.fixture
def loop():
    # disable global event loop
    asyncio.set_event_loop(None)
    yield asyncio.new_event_loop()


def test_lift_creation():
    lift = Lift(1)
    assert lift


def test_next_action_process(loop):
    lift = Lift(1)

    q = asyncio.Queue(loop=loop)

    async def test_process(*args, **kwargs):
        raise OKException

    lift.process = test_process

    # test that process was called for every state
    for state in (States.UP, States.DOWN, States.IDLE, States.OPEN):
        lift.state = state
        with pytest.raises(OKException):
            loop.run_until_complete(next_action(lift, q))

    async def test_process(*args, **kwargs):
        pass

    async def test_make_transition(state_toward, user_state):
        # user wants to floor below, but lift must go up to pick him up
        assert state_toward == States.UP
        assert user_state == States.DOWN
        raise OKException

    lift.state = States.IDLE
    lift.process = test_process
    lift.make_transition = test_make_transition

    asyncio.ensure_future(q.put((States.DOWN, 10, )), loop=loop)

    with pytest.raises(OKException):
        loop.run_until_complete(next_action(lift, q))

    # now user wants to floor below, but lift must go down to pick him up
    asyncio.ensure_future(q.put((States.DOWN, 10, )), loop=loop)
    lift.current_floor = 20

    async def test_make_transition2(state_toward, user_state):
        assert state_toward == States.DOWN
        assert user_state == States.DOWN
        raise OKException
    lift.make_transition = test_make_transition2

    with pytest.raises(OKException):
        loop.run_until_complete(next_action(lift, q))


def test_process_up(loop):
    lift = Lift(0.1, 0.1)
    lift.state = States.UP
    lift.call_to_floor(2, States.UP)
    lift.call_to_floor(3, States.DOWN)

    assert lift.top == 3
    assert lift.bottom == 2

    async def test_process(lift):
        # moved to second floor, waiters still there
        await lift.process_up()
        assert lift.current_floor == 2
        assert lift.want_up[2]

        # next step - take all waiters
        await lift.process_up()
        assert lift.state == States.UP
        assert 2 not in lift.want_up

        # next step - go to 3 floor, wanting users here
        await lift.process_up()
        assert lift.current_floor == 3
        assert lift.want_down[3]

        # someone called lift from 1nd floor
        lift.call_to_floor(1, States.UP)

        # next step - lift changed his direction (because it achieved top
        # requested floor), take waiters and ready to go down
        await lift.process_up()
        assert lift.state == States.DOWN
        assert lift.current_floor == 3

    loop.run_until_complete(test_process(lift))
